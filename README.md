# Global Assets
The Global Assets folder stores files that are used by the many different components of the system.

# parser
The parser folder stores the different parsing components that filter, convert and produce a constrained json document from a srcML XML.

# Middleware - Folder
The Middleware folder stores the components that deal with the functions and services of the Middleware.

# Middleware - Shortcut
The Middleware shortcut file targets the .exe file know as ConsoleApplication.exe located in the Middleware/Pipes/bin/Debug folder.

# GitFilter
The GitFilter folder contains the components that deal with retriving projects from a Git repo, and activating srcML.